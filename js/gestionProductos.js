var productosObtenidos;

function getProductos()
{
  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function()
  {
    if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       //console.log(request.responseText);
       productosObtenidos = request.responseText;
       procesarProductos();
    }
  };

  request.open("GET", url, true);
  request.send();

}

function procesarProductos()
{
  var JSONproductos = JSON.parse(productosObtenidos);
  var tabla =document.getElementById("tablaProductos");

  for (var i = 0; i < JSONproductos.value.length; i++)
  {
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONproductos.value[i].ProductName;
    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONproductos.value[i].UnitPrice;
    var columnaStock = document.createElement("td");
    columnaStock.innerText = JSONproductos.value[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);
    tabla.appendChild(nuevaFila);

    /*
    console.log(JSONproductos.value[i].ProductName);
    console.log(JSONproductos.value[i].UnitPrice);
    console.log(JSONproductos.value[i].UnitsInStock);
    */
  }
}
