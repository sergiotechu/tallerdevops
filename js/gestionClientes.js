var clientesObtenidos;

function getClientes() {
  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function()
  {
    if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       //console.log(request.responseText);
       clientesObtenidos = request.responseText;
       procesarClientes();
    }
  };

  request.open("GET", url, true);
  request.send();

}

function procesarClientes()
{
  var JSONclientes = JSON.parse(clientesObtenidos);

  var tabla = document.createElement("table");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  tabla.classList.add("col-lg-12");
  var tablaHead = document.createElement("thead");

  var tablaHeadElement1 = document.createElement("th");
  tablaHeadElement1.innerText = "Nombre";
  tablaHead.appendChild(tablaHeadElement1);

  var tablaHeadElement2 = document.createElement("th");
  tablaHeadElement2.innerText = "Telefono";
  tablaHead.appendChild(tablaHeadElement2);

  var tablaHeadElement3 = document.createElement("th");
  tablaHeadElement3.innerText = "Direccion";
  tablaHead.appendChild(tablaHeadElement3);

  var tablaHeadElement4 = document.createElement("th");
  tablaHeadElement4.innerText = "Pais";
  tablaHead.appendChild(tablaHeadElement4);

  tabla.appendChild(tablaHead);

  for (var i = 0; i < JSONclientes.value.length; i++)
  {
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONclientes.value[i].ContactName;

    var columnaTelefono = document.createElement("td");
    columnaTelefono.innerText = JSONclientes.value[i].Phone;

    var columnaDireccion = document.createElement("td");
    columnaDireccion.innerText = JSONclientes.value[i].Address;

    var imagenBandera = document.createElement("img");
    if (JSONclientes.value[i].Country == 'UK') {
      imagenBandera.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png";
    } else {
      imagenBandera.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-" + JSONclientes.value[i].Country + ".png";
    }
    imagenBandera.classList.add("flag");
    var columnaBandera = document.createElement("td");
    columnaBandera.appendChild(imagenBandera);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaTelefono);
    nuevaFila.appendChild(columnaDireccion);
    nuevaFila.appendChild(columnaBandera);

    tabla.appendChild(nuevaFila);

    var divClientes = document.getElementById("divClientes");
    divClientes.appendChild(tabla);

  }
}
